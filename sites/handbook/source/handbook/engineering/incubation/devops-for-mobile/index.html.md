---
layout: handbook-page-toc
title: DevOps for Mobile Apps Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## DevOps for Mobile Apps Single-Engineer Group

The DevOps for Mobile Apps SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation). This group is focused on [our
direction](/direction/mobile/mobile-devops/) for DevOps for Mobile Applications.

Our goal is to improve the experience for Developers targeting mobile platforms by providing CI/CD capabilities and workflows that improve the experience of provisioning and deploying mobile apps written with native iOS and Android technologies.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329592](https://gitlab.com/gitlab-org/gitlab/-/issues/329592)
