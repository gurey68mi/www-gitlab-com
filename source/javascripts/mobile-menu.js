(function() {
  let mobileNavIsActive = false;
  let ul = document.getElementsByClassName("navigation-mobile__dropdown")[0]
  let div = document.getElementsByClassName("navigation-mobile")[0]
  
  //Event handler for clicking hamburger menu
  document.getElementById('slpMobileNavActive').onclick = function(){
    if(!mobileNavIsActive){
      ul.classList.remove("navigation-mobile__dropdown")
      ul.classList.add("navigation-mobile__dropdown-active")
      div.classList.remove("navigation-mobile")
      div.classList.add("navigation-mobile-active")
      mobileNavIsActive = !mobileNavIsActive
    }
    else {
      ul.classList.add("navigation-mobile__dropdown")
      ul.classList.remove("navigation-mobile__dropdown-active")
      div.classList.add("navigation-mobile")
      div.classList.remove("navigation-mobile-active")
      mobileNavIsActive = !mobileNavIsActive
    }
  }

  let buttons = document.querySelectorAll(".navigation-mobile__button");
  buttons.forEach(element => element.onclick = function(){
    if(element.nextElementSibling.classList.contains('navigation-mobile__item')){
      element.nextElementSibling.classList.remove('navigation-mobile__item')
      element.nextElementSibling.classList.add("navigation-mobile__item-active")
    }
    else{
      element.nextElementSibling.classList.add('navigation-mobile__item')
      element.nextElementSibling.classList.remove("navigation-mobile__item-active")
    }
  });


  //event handler for drop down mobile menus

})();
